const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');
const adminController = require('../../controllers/admin/admin');

router.post('/login', adminController.adminLogin);

router.post('/registration', adminController.adminRegister);

module.exports = router;
 