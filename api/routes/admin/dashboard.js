const express = require('express');
const router = express.Router();

const auth = require('../../middleware/auth');
const dashboardController = require('../../controllers/admin/dashboard');

router.post('/', dashboardController.verifyToken)


module.exports = router;
 