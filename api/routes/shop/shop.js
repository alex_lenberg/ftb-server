const express = require('express');
const router = express.Router();

// const Product = require('../../models/product');
const shopController = require('../../controllers/shop/shop');

router.get('/', shopController.getProducts);

router.get('/:productId', shopController.getProduct);

module.exports = router;
 