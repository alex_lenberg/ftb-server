const express = require('express');
const router = express.Router();

const userController = require('../../controllers/shop/user');

router.get('/verify', userController.getVerifiedUser);

router.post('/login', userController.userSignIn);

router.post('/register', userController.userSignUp);

module.exports = router;
 