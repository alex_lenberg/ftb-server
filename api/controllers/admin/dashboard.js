const User = require('../../models/user');
const Product = require('../../models/product');

const jwt = require('jsonwebtoken');

// exports.getDashboard = (req, res, next) => {

// };

module.exports.verifyToken = (req, res, next) => {
  try{
    const token = req.body.token;
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    if(decoded){
      res.status(201).send(true);      
    }    
  }
  catch (error){
    res.status(401).send(false);
  }
}