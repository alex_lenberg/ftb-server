const User = require('../../models/user');
const Sequelize = require('sequelize');

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.adminLogin = (req, res, next) => {
  User.findAll({where: { email: req.body.email }})
  .then( admins => {
    if(admins.length < 1){
      res.status(404).json({
        message: 'Auth failed'
      });
    } 
    else if(admins.length === 1){
      const ifSame = bcrypt.compareSync(req.body.password, admins[0].password);
      if(ifSame){
        const token = jwt.sign(
          {
            email: admins[0].email,
            userId: admins[0].id
          }, 
          process.env.JWT_KEY, 
          {
            expiresIn: "2h"
          }
        )
        res.status(201).json({
          token,
          message: 'Auth successful'
        });
      }
      else{
        res.status(401).json({
          message: 'Email or password are wrong'
        });
      }      
    }
    else{
      res.status(500).json({
        message: 'There are few users with this email address. Ask technical support.'
      });
    }   
  })
  .catch( err => {
    res.status(500).json({
      message: `Error. ${err}`
    })
  });
};


exports.adminRegister = (req, res, next) => {
  User.findOrCreate( {where: {email: req.body.email}, defaults: {
    password: bcrypt.hashSync(req.body.password, 10),
    role: req.body.role ? req.body.role : 1,
    name: 'User'
  }}).spread( (user, ifCreated) => {
    if(ifCreated){
      res.send(user);
    }
    else{
      res.status(401).json({
        message: 'Auth failed'
      });
    }
  });
};


exports.getMain = (req, res, next) => {
  
}