const Product = require('../../models/product');

exports.getProducts = (req, res, next) => {
  Product.findAll()
  .then( products => {
    res.send(products);
  })
  .catch( err => {
    console.log('error--->', err)
  });
};

exports.getProduct = (req, res, next) => {
  const prodId = req.params.productId;
  Product.findByPk(+prodId)
  .then(product => {
    res.send(product);
  })
  .catch( err => console.log(err) )
};