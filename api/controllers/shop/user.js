const User = require('../../models/user');

const Sequelize = require('sequelize');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

exports.userSignIn = (req, res, next) => {
  User.findAll({where: { email: req.body.email }})
  .then( users => {    
    if(users.length < 1){
      res.status(404).json({
        message: 'Auth failed'
      });
    } 
    else if(users.length === 1){
      const ifSame = bcrypt.compareSync(req.body.password, users[0].password);
      if(ifSame){
        const token = jwt.sign(
          {
            email: users[0].email,
            userId: users[0].id
          }, 
          process.env.JWT_KEY, 
          { expiresIn: "12h" }
        );
        res.status(201).json({
          token,
          message: 'Auth successful'
        });
      }
      else{
        res.status(401).json({
          message: 'Email or password are wrong'
        });
      }      
    }
    else{
      res.status(500).json({
        message: 'There are few users with this email address. Ask technical support.'
      });
    }   
  })
  .catch( err => {
    res.status(500).json({
      message: `Error. ${err}`
    })
  });
};

exports.userSignUp = (req, res, next) => {
  User.findOrCreate({
    where: {email: req.body.email}, 
    defaults: {
      password: bcrypt.hashSync(req.body.password, 10),
      role: req.body.role ? req.body.role : 2,
      first_name: req.body.first_name,
      last_name: req.body.last_name,
    }
  }).spread( (user, ifCreated) => {
    if(ifCreated){
      res.send(user);
    }
    else{
      res.status(401).json({
        message: 'Auth failed'
      });
    }
  });
};

exports.getVerifiedUser = (req, res, next) => {
  try{
    const token = req.headers.authorization;
    if(token){    
      const decoded = jwt.verify(token, process.env.JWT_KEY);      
      User.findByPk(decoded.userId)
        .then(user => {        
          res.status(201).send({
            first_name: user.first_name,
            last_name: user.last_name,
            email: user.email,
            role: user.role
          });
        })      
    }
  }
  catch(error){
    if(error.name === 'TokenExpiredError'){
      res.status(401).send({
        message: error.message,
        tokenExpired: true
      });
    }
    else{
      res.status(500).json({
        message: 'Internal server error.'
      });
    }
  }
  
};