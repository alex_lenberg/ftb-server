const jwt = require('jsonwebtoken');

module.exports.verifyToken = (req, res, next) => {
  try{
    const token = req.headers.authorization.split(" ")[1];
    const decoded = jwt.verify(token, process.env.JWT_KEY);
    req.userData = decoded;
    console.log(req);
    next();
  }
  catch (error){
    res.status(401).json({
      message: 'Auth failed. Try to login.'
  });
  }
}