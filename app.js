const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser')
const cors = require('cors');

const sequelize = require('./util/database');

const app = express();

const auth = require('./api/middleware/auth');

const shopRouter = require('./api/routes/shop/shop');
const userRouter = require('./api/routes/shop/user');

const adminRouter = require('./api/routes/admin/admin');
const dashboardRouter = require('./api/routes/admin/dashboard');

app.use(morgan('dev'));
app.use(express.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));


app.use('/api/products', shopRouter);
app.use('/api/user', userRouter);

app.use('/api/admin', adminRouter);
app.use('/api/dashboard', dashboardRouter);

sequelize.sync()
  .then( () => {
    app.listen(3000);
  })
  .catch(err => {
    console.log(err);
  });

