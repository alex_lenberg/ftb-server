const Sequelize = require('sequelize');

const sequelize = new Sequelize(
  'ftb-server', 
  'root', 
  'password', {
  dialect: 'mysql',
  host: 'localhost',
})

module.exports = sequelize;
